//var connect = require("connect");
var assert = require("assert");

module.exports = function startup(options, imports, register) {

    assert(options.key, "option 'key' is required");
    assert(options.secret, "option 'secret' is required");

    var connect = imports.connect;
		var MongoStore = require('connect-mongo')(connect);
    var sessionStore = imports["session-store"];

    connect.useSession(Session({
        store: MongoStore,
        key: options.key,
        secret: options.secret
    }));

    register(null, {
        session: {
            getKey: function() {
                return options.key;
            },
            get: sessionStore.get
        }
    });
};
