var Session = require("connect").session;
var assert = require("assert");



module.exports = function startup(options, imports, register) {

	assert(options.key, "option 'key' is required");
	assert(options.secret, "option 'secret' is required");

	var connect = imports.connect;
	var _connect = require('connect');

	var MySQLStore = require('connect-mysql')(_connect);

	options = {
		table: 'c9_sessions',
		config: {
			host: 'localhost',
			user: 'naomay_c9',
			password: '!unlock123!',
			database: 'naomay_c9'
		}
	};



	var sessionStore = new MySQLStore(options);
//	console.log(options.secret);
//	connect.session({secret: options.secret});
	connect.useSession(Session({
		key: 'c9connect.sid',
		secret: 'v1234',
		store: sessionStore,
		proxy: true
	}));

//	MongoStore.prototype.get = function (sid, fn) {
//		var self = this;
//		MongoClient.connect('mongodb://127.0.0.1:27017/' + options.db, function (err, db) {
//			if (err) {
//				console.log("Mongo connection error:", err);
//				fn && fn(err);
//			} else {
//				var sess;
//				var sessions = db.collection('sessions');
////				var ObjectID = require('mongodb').ObjectID;
//				console.log("Find by sid", sid);
//				sessions.findOne({sid: sid}, function (err, result) {
//					if (err) {
//						fn && fn(err);
//					} else {
//						try {
//							sess = JSON.parse(result);
//							console.log("Session:", sess);
//						} catch (e) {
//							console.warn("Error '" + e + "' reading session: " + sid, result);
//							//@todo destroy session
//							return;
//						}
//						if (sess) {
//							var expires = (typeof sess.cookie.expires === 'string')
//								? new Date(sess.cookie.expires)
//								: sess.cookie.expires;
//							if (!expires || new Date < expires) {
//								fn(null, sess);
//							} else {
//								//@todo destroy
//								fn(null, sess);
//							}
//						}
//					}
//				});
//			}
//		});
//	};
//	console.log(sessionStore);
	console.log("Mysql store:", sessionStore.get);


	getSess = function(sid,cb){
		var mysql = require('mysql');
		var connection = mysql.createConnection({
			host: 'localhost',
			user: 'naomay_c9',
			password: '!unlock123!',
			database: 'naomay_c9'
		});
		connection.query("SELECT * from c9_sessions WHERE sid =?",[sid], function(err,result){
			if(!err && result && result.length>0){
				try{
					var sess = JSON.parse(result[0]['session']);
					cb(null, sess);
					connection.end();
				}catch(err){
					console.log("Session error:", err);
					cb(err);
					connection.end();
				}
			}else{
				cb(err);
				connection.end();
			}
		});
	}

	register(null, {
		session: {
			getKey: function () {
				return options.key;
			},
			get: getSess
		}
	});
};
