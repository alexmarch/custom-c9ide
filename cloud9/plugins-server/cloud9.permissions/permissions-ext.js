var User = require("../cloud9.core/user");
var cookie = require('cookie');



module.exports = function startup(options, imports, register) {

	var connect = imports.connect;
	var _session = imports.session;
	var _connect = require('./connect');

	var table = options.table,
		host = options.host,
		user = options.user,
		password = options.password,
		database = options.database;

	connect.useSession(function (req, res, next) {
		var mysql = require('mysql');
		var connection = mysql.createConnection({
			host: host,
			user: user,
			password: password,
			database: database,
			connectTimeout: 2000
		});

		if (!req.session.uid) {
			var cookies = cookie.parse(req.headers.cookie);
			var sessionID = _connect.utils.parseSignedCookie(cookies['connect.sid'], 'v1234');
			console.log("SESSIONID:",sessionID);
			if(!sessionID){
				connection.end();
				next();
				return;
			}
			connection.query("SELECT * from "+table+" WHERE sid =?",[sessionID],function(err,result){
				console.log("QUERY:",err,result);
				if(!err && result && result.length>0){
					try{
						var sess = JSON.parse(result[0]['session']);
						if(sess && sess.uid){
							req.session.userid = sess.uid;
							req.session.uid = req.sessionID;
							connection.end();
							next();
						}else{
							connection.end();
							next();
						}
					}catch(err){
						console.log("Session error:", err);
						eq.session.uid = null;
						connection.end();
					}
				}else{
					console.log("Query result error", err);
					req.session.uid = null;
					connection.end();
				}
			});
		}else{
			console.log("elace next");
			connection.end();
			next();
		}
//		next();
	});

	register(null, {
		"workspace-permissions": {
			getPermissions: function (uid, workspaceId, origin, callback) {
				if (!uid)
					return callback(new Error("Invalid user id: " + uid));

				return callback(null, User.OWNER_PERMISSIONS);
			},
			getRole: function (uid, workspaceId, callback) {
				return callback(null, 1);
			},
			getConfig: function () {
				return {};
			}
		}
	});
};
