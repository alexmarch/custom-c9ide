'use strict';

var spawn = require('child_process').spawn,
	config = require('../config/global');

exports.projects = {
	editor: function (req, res) {
		//@todo temporary fix
		if(req.session.ide){
			return res.redirect('/ide/?p='+rndPort);
		}
		var rndPort = 3131;//Math.floor((Math.random(new Date().getTime()) * 9000) + 1000);
		var c9ide = spawn(config.c9path, ['-l', '127.0.0.1', '-p', rndPort, '-w', '/home/naomay/git/project/']);
		req.session.ide = c9ide.pid;
		req.session.port = rndPort;
		c9ide.stdout.on('data', function (data) {
			if( data.toString().indexOf('initialized.') !== -1){
				res.redirect('/ide/?p='+rndPort);
			}
		});
	}
}
